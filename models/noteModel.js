const mongoose = require('mongoose');
const schema = mongoose.Schema;

const noteSchema = new mongoose.Schema({
  userId: {
    type: schema.Types.ObjectId,
    ref: 'User',
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: String,
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Note = mongoose.model('Note', noteSchema);
