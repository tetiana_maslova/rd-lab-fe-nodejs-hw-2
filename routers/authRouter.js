const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers');
const {validateRegistr} = require('./middleWares/validationMiddleware');
const {
  login,
  registr,
} = require('../controllers/authController');

router
    .post('/register', asyncWrapper(validateRegistr), asyncWrapper(registr));

router.post('/login', asyncWrapper(validateRegistr), asyncWrapper(login));

module.exports = router;
