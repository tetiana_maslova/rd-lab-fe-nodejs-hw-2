const express = require('express');
const usersRouter = new express.Router();
const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middleWares/authMiddleware');

const {
  getUserInfo,
  deleteUser,
  changePassword,
} = require('../controllers/usersController');

usersRouter
    .get('/me', asyncWrapper(authMiddleware), asyncWrapper(getUserInfo));

usersRouter
    .patch('/me', asyncWrapper(authMiddleware), asyncWrapper(changePassword));

usersRouter
    .delete('/me', asyncWrapper(authMiddleware), asyncWrapper(deleteUser));

module.exports = usersRouter;
