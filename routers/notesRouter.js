const express = require('express');
const notesRouter = new express.Router();
const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middleWares/authMiddleware');

const {
  getNotes,
  postNote,
  getById,
  putById,
  patch,
  remove,
} = require('../controllers/notesController');

notesRouter.get('/', asyncWrapper(authMiddleware), asyncWrapper(getNotes));

notesRouter.post('/', asyncWrapper(authMiddleware), asyncWrapper(postNote));

notesRouter.get('/:id', asyncWrapper(authMiddleware), asyncWrapper(getById));

notesRouter.put('/:id', asyncWrapper(authMiddleware), asyncWrapper(putById));

notesRouter.patch('/:id', asyncWrapper(authMiddleware), asyncWrapper(patch));

notesRouter.delete('/:id', asyncWrapper(authMiddleware), asyncWrapper(remove));

module.exports = notesRouter;
