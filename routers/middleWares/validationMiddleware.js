const joi = require('joi');

module.exports.validateRegistr = async (req, res, next) => {
  const schema = joi.object({
    username: joi.string()
        .required(),

    password: joi.string()
        .required(),
  });

  await schema.validateAsync(req.body);
  next();
};
