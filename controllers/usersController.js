const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

module.exports.getUserInfo = async (req, res) => {
  const user = await User.findById(req.user._id, {
    __v: 0,
    password: 0,
  });

  return res.status(200).json({
    user,
  });
};

module.exports.deleteUser = async (req, res) => {
  await User.findByIdAndDelete(req.user._id);
  return res.status(200).json({
    message: 'Success',
  });
};

module.exports.changePassword = async (req, res) => {
  const {
    oldPassword,
    newPassword,
  } = req.body;

  if (!oldPassword || !newPassword) {
    throw new Error('Specify password fields!');
  }

  await User.findByIdAndUpdate(req.user._id, {
    $set: {
      password: await bcrypt.hash(newPassword, 10),
    },
  });

  return res.status(200).json({
    message: 'Success',
  });
};
