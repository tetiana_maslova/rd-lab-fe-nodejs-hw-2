const mongoose = require('mongoose');
const {User} = require('../models/userModel');
const jwt = require('jsonwebtoken');
const JWT_SECRET = process.env.JWT_SECRET;
const bcrypt = require('bcrypt');

module.exports.registr = async (req, res) => {
  const {
    username,
    password,
  } = req.body;

  const user = new User({
    _id: new mongoose.Types.ObjectId(),
    username,
    password: await bcrypt.hash(password, 10),
  });

  await user.save();

  res.status(200).json({
    message: 'Success',
  });
};

module.exports.login = async (req, res) => {
  const {
    username,
    password,
  } = req.body;

  const user = await User.findOne({
    username,
  });

  if (!user) {
    return res.status(400).json({
      message: `No user with username ${username} found.`,
    });
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({
      message: `Wrong password.`,
    });
  }

  const token = jwt.sign({
    username: user.username,
    _id: user._id,
  }, JWT_SECRET);

  res.status(200).json({
    message: 'success',
    jwt_token: token,
  });
};
