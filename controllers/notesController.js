const {Note} = require('../models/noteModel');

module.exports.getNotes = async (req, res) => {
  const user = req.user;
  const {
    offset = 0, limit = 5,
  } = req.query;

  Note
      .find({
        userId: user._id,
      }, {
        __v: 0,
      }, {
        skip: parseInt(offset, 10),
        limit: limit > 100 ? 5 : parseInt(limit, 10),
      })
      .populate({
        path: 'notes',
      })
      .exec((err, notes) => {
        if (err) {
          throw err;
        }
        return res.status(200).json({
          notes,
        });
      });
};

module.exports.postNote = async (req, res) => {
  const user = req.user;

  if (!req.body.text) {
    return res.status(400).json({
      message: 'Please specify text param',
    });
  }

  await user.save(async (err) => {
    if (err) {
      return res.status(500).json({
        message: err.message,
      });
    }

    try {
      const note = new Note({
        text: req.body.text,
        userId: user._id,
      });

      await note.save();

      return res.status(200).json({
        message: 'Success',
      });
    } catch (error) {
      return res.status(500).json({
        message: error.message,
      });
    }
  });
};

module.exports.getById = async (req, res) => {
  if (!req.params.id) {
    return res.status(400).json({
      message: 'Id param is required!',
    });
  }
  const user = req.user;

  try {
    await Note.findById(req.params.id);
  } catch (err) {
    return res.status(400).json({
      message: 'No note with specified id.',
    });
  }

  Note
      .findOne({
        $and: [{
          userId: user._id,
        },
        {
          _id: req.params.id,
        },
        ],
      }, {
        __v: 0,
      })
      .populate({
        path: 'notes',
      })
      .exec((err, note) => {
        if (err) {
          throw err;
        }

        if (!note) {
          return res.status(400).json({
            message: 'No note with specified id.',
          });
        }

        return res.status(200).json({
          note,
        });
      });
};

module.exports.putById = async (req, res) => {
  if (!req.params.id) {
    return res.status(400).json({
      message: 'Id param is required!',
    });
  }

  if (!req.body.text) {
    return res.status(400).json({
      message: 'Text param is required!',
    });
  }
  await Note.findByIdAndUpdate(req.params.id, {
    $set: req.body,
  });

  return res.status(200).json({
    message: 'success',
  });
};

module.exports.patch = async (req, res) => {
  const note = await Note.findById(req.params.id);
  if (!note) {
    return res.status(400).json({
      message: 'Id param is required!',
    });
  }

  let noteStatus;

  if (note.completed) {
    noteStatus = false;
  } else {
    noteStatus = true;
  }
  await Note.findByIdAndUpdate(req.params.id, {
    $set: {
      completed: noteStatus,
    },
  });

  return res.status(200).json({
    message: 'success',
  });
};

module.exports.remove = async (req, res) => {
  if (!req.params.id) {
    return res.status(400).json({
      message: 'Id param is required!',
    });
  }
  await Note.findByIdAndDelete(req.params.id);

  return res.status(200).json({
    message: 'success',
  });
};
